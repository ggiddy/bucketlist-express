FROM node:8-slim
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN npm install
EXPOSE 3500
CMD npm run dev
