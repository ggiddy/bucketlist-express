module.exports = {
  env: {
    node: true,
    jest: true,
    commonjs: true,
    es6: true
  },
  extends: 'eslint:recommended',
  parser: 'babel-eslint',
  plugins: [
    'import',
    'jest',
    'flowtype'
  ],
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  globals: {
    process: true,
    __dirname: true,
    describe: true
  },
  rules: {
    'jest/no-focused-tests': 2,
    'jest/no-identical-title': 2,
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'never']
  }
}
