export default function(sequelize, DataTypes) {
  const Bucketlist = sequelize.define('Bucketlist', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })

  Bucketlist.associate = (models) => {
    Bucketlist.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'Owner',
      onDelete: 'CASCADE'
    })

    Bucketlist.hasMany(models.BucketlistItem, {
      foreignKey: 'bucketlistId',
      as: 'Items',
      sourceKey: 'id',
      onDelete: 'CASCADE'
    })
  }

  return Bucketlist
}
