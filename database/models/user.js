export default function(sequelize, DataTypes) {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  })

  User.associate = (models) => {
    User.hasMany(models.Bucketlist, {
      foreignKey: 'userId',
      as: 'Bucketlists',
      sourceKey: 'id',
      onDelete: 'CASCADE'
    })
  }

  return User
}
