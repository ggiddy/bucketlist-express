export default function(sequelize, DataTypes) {
  const BucketlistItem = sequelize.define('BucketlistItem', {
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    bucketlistId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })

  BucketlistItem.associate = (models) => {
    BucketlistItem.belongsTo(models.Bucketlist, {
      foreignKey: 'bucketlistId',
      as: 'Bucketlist',
      onDelete: 'CASCADE'
    })
  }

  return BucketlistItem
}
