module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('BucketlistItems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      bucketlistId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Bucketlists',
          key: 'id',
          as: 'bucketlistId'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('BucketlistItems')
  }
}
